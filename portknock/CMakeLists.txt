add_library(libportknock portknock.cpp)
target_link_libraries(libportknock cactus)

add_catch(test_portknock portknock_test.cpp)
target_link_libraries(test_portknock libportknock)

add_executable(portknock main.cpp)
target_link_libraries(portknock libportknock)