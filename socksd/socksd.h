#pragma once

#include <cactus/cactus.h>

class SocksServer {
public:
    explicit SocksServer(const folly::SocketAddress& at);

    void Serve();

private:
    std::unique_ptr<cactus::IListener> lsn_;
    cactus::ServerGroup group_;
};