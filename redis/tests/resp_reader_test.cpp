#include <catch2/catch.hpp>

#include <redis/resp_reader.h>

#include <cactus/io/reader.h>

using namespace cactus;
using namespace redis;

TEST_CASE("RESP reader") {
    SECTION("SimpleString") {
        ViewReader reader(View("+OK\r\n"));
        RespReader resp_reader(&reader);
        REQUIRE(resp_reader.ReadType() == ERespType::SimpleString);
        REQUIRE(resp_reader.ReadSimpleString() == "OK");
    }

    SECTION("Error") {
        ViewReader reader(View("-ERR Some error\r\n"));
        RespReader resp_reader(&reader);
        REQUIRE(resp_reader.ReadType() == ERespType::Error);
        REQUIRE(resp_reader.ReadError() == "ERR Some error");
    }

    SECTION("Int") {
        ViewReader reader(View(":42\r\n"));
        RespReader resp_reader(&reader);
        REQUIRE(resp_reader.ReadType() == ERespType::Int);
        REQUIRE(resp_reader.ReadInt() == 42);
    }

    SECTION("BulkString") {
        ViewReader reader(View("$11\r\nBulk string\r\n"));
        RespReader resp_reader(&reader);
        REQUIRE(resp_reader.ReadType() == ERespType::BulkString);
        REQUIRE(resp_reader.ReadBulkString() == "Bulk string");
    }

    SECTION("Null BulkString") {
        ViewReader reader(View("$-1\r\n"));
        RespReader resp_reader(&reader);
        REQUIRE(resp_reader.ReadType() == ERespType::BulkString);
        REQUIRE(!resp_reader.ReadBulkString().has_value());
    }

    SECTION("Array") {
        ViewReader reader(View(
            "*2\r\n"
            ":173\r\n"
            "$3\r\nYES\r\n"));
        RespReader resp_reader(&reader);
        REQUIRE(resp_reader.ReadType() == ERespType::Array);
        REQUIRE(resp_reader.ReadArrayLength() == 2);
        REQUIRE(resp_reader.ReadType() == ERespType::Int);
        REQUIRE(resp_reader.ReadInt() == 173);
        REQUIRE(resp_reader.ReadType() == ERespType::BulkString);
        REQUIRE(resp_reader.ReadBulkString() == "YES");
    }

    SECTION("Null array") {
        ViewReader reader(View("*-1\r\n"));
        RespReader resp_reader(&reader);
        REQUIRE(resp_reader.ReadType() == ERespType::Array);
        REQUIRE(resp_reader.ReadArrayLength() == -1);
    }
}
