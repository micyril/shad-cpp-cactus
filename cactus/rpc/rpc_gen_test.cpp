#include <cactus/test.h>

#include <cactus/rpc/rpc_test.pb.h>

using namespace cactus;

TEST_CASE("Example message") {
    TestRequest req;
    req.set_query("ping");

    TestResponse rsp;
    rsp.set_results("pong");
}

class TestChannel : public IRpcChannel {
public:
    void CallMethod(const google::protobuf::MethodDescriptor* method,
                    const google::protobuf::Message& request,
                    google::protobuf::Message* response) override {

        REQUIRE(method->name() == "Search");

        auto req = dynamic_cast<const TestRequest&>(request);
        auto rsp = dynamic_cast<TestResponse*>(response);

        rsp->set_results(req.query());
    }
};

TEST_CASE("Check client codegen") {
    TestChannel channel;

    TestServiceClient stub(&channel);

    TestRequest req;
    req.set_query("foobar");

    auto rsp = stub.Search(req);
    REQUIRE("foobar" == rsp->results());
}

class TestServiceImpl : public TestServiceHandler {
public:
    int searched = 0;
    int pinged = 0;

    void Search(const TestRequest& request, TestResponse* response) override {
        searched++;
    }

    void Ping(const PingRequest& request, PingResponse* response) override {
        pinged++;
    }
};

TEST_CASE("Check server codegen") {
    TestServiceImpl impl;
    TestServiceClient client(&impl);

    client.Search(TestRequest{});
    REQUIRE(1 == impl.searched);
    REQUIRE(0 == impl.pinged);

    client.Ping(PingRequest{});
    REQUIRE(1 == impl.searched);
    REQUIRE(1 == impl.pinged);
}

TEST_CASE("Example descriptors") {
    TestServiceImpl handler;
    auto descriptor = handler.ServiceDescriptor();

    REQUIRE(descriptor->name() == "TestService");
    REQUIRE(descriptor->full_name() == "cactus.TestService");

    auto method = descriptor->FindMethodByName("Search");
    REQUIRE(method->name() == "Search");
    REQUIRE(method->full_name() == "cactus.TestService.Search");

    // default instance of message.
    auto request_prototype =
        google::protobuf::MessageFactory::generated_factory()->GetPrototype(method->input_type());

    // new request object.
    std::unique_ptr<google::protobuf::Message> request(request_prototype->New());
    REQUIRE(request->GetTypeName() == "cactus.TestRequest");
}
