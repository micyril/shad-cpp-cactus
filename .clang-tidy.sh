#!/usr/bin/sh

find cactus examples -name '*.cpp' | xargs clang-tidy -fix -p build/debug
